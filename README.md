Home Assistant system for Raspbian
==================================

This projet defines a Home assistant instalation for Raspbian (Rasberry Pi) based on Docker-compose. A simgle command can start up the project

The sistem in integrated with:

- Esphome: a platform to easy configure esp boards like the popular sonoff
- Mqtt: the popular message broker for IoT.
- Node-red: an auxiliar system for graphic program the interaction beteen devices and other integrations

All those system al also included as docker, the configuration of all systems is in the *volumes* folder

**Warning**
If cloning this repo, thake care of not pushing your secrets to public Internet.

Starting up 
========================

Single command needed

```bash
docker-compose up
```

Note: your requisites are docker and docker-compose installed


Starting up as a service
========================

If you want to start docker compse as services you can place your docker compose on `/home/pi/docker` an define a generic service as following.

create `/etc/systemd/system/docker-compose@.service` with:

```shell
[Unit]
Description=%i service with docker compose
Requires=docker.service
After=docker.service

[Service]
Restart=always
User=pi

WorkingDirectory=/home/pi/docker/%i

# Remove old containers, images and volumes
#ExecStartPre=/usr/local/bin/docker-compose down -v
#ExecStartPre=/usr/local/bin/docker-compose rm -fv
#ExecStartPre=-/bin/bash -c 'docker volume ls -qf "name=%i_" | xargs docker volume rm'
#ExecStartPre=-/bin/bash -c 'docker network ls -qf "name=%i_" | xargs docker network rm'
#ExecStartPre=-/bin/bash -c 'docker ps -aqf "name=%i_*" | xargs docker rm'

# Compose up
ExecStart=/usr/local/bin/docker-compose up

# Compose down, remove containers and volumes
#ExecStop=/usr/local/bin/docker-compose down -v
ExecStop=/usr/local/bin/docker-compose stop

[Install]
WantedBy=multi-user.target
```
[source](https://www.adictosaltrabajo.com/2018/04/03/ejecutar-cualquier-docker-compose-como-servicio/)

If this proyect is cloned on /home/pi/docker/home then you can create and stop

```bash 
sudo systemctl start docker-compose@home
sudo systemctl stop docker-compose@home
```

When happi enable the service (to autostart) with

```bash
sudo systemctl enable docker-compose@home
```

